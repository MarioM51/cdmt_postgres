# Postgres CDMT

Iniciar servicio: `docker-compose -f ./docker-compose-postgres.yml up -d`

Detener servicio: `docker-compose -f ./docker-compose-postgres.yml down`

Entrar a BD: `psql -h 192.168.1.76 -p 5000 -U mario_prod -d cdmt_prod`

Crear respaldo 

```r
docker exec cdmt_postgres_prod pg_dump -U mario_prod cdmt_prod > "backup.sql"
```

Levantar contendor con respaldo

```r
docker run --name pg_prueba --rm \
  -v //my-backup:/docker-entrypoint-initdb.d \
  -ti postgres:13.2-alpine sh
```

