#!/bin/bash

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
set -o allexport; source $SCRIPTPATH/../.env; set +o allexport -e

docker exec $POSTGRES_CONTAINER_NAME \
  pg_dump --format plain -U $POSTGRES_USER -d $POSTGRES_DB_NAME > \
  "$SCRIPTPATH/../backups/dump.pgdata"
